import firebase from 'firebase/app';
import 'firebase/auth'
import 'firebase/database';
import 'firebase/storage';


const firebaseConfig = {
    apiKey: "AIzaSyBUdjqWz_ac7ZEjrwqQ3UqxT7wm13mbzXc",
    authDomain: "react-auth-hasson.firebaseapp.com",
    databaseURL: "https://react-auth-hasson.firebaseio.com",
    projectId: "react-auth-hasson",
    storageBucket: "react-auth-hasson.appspot.com",
    messagingSenderId: "237494599729",
    appId: "1:237494599729:web:403ed6ea1e7da444f75510"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
